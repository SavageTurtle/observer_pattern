#include "observer.hpp"

observer::~observer()
{

}

void observer::update()
{
}

void observer::remove_me()
{
    sub.detach(this);
}


observer::observer(subject& subj):
    sub(subj)
{
    sub.attach(this);
}