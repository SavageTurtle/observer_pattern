#include "feeder.hpp"

feeder::feeder()
{
    // tu asi netreba nic
}

void feeder::registerForFood(IObserver* an)
{
    this->attach(an);
}

void feeder::isFed(IObserver* an)
{
    this->detach(an);
}

void feeder::feed()
{
    this->notify();
}

void feeder::attach(IObserver* observer)
{
    observers.push_back(observer);
}

void feeder::detach(IObserver* observer)
{
    observers.remove(observer);
}

void feeder::notify()
{
    std::list<IObserver*>::iterator iter = observers.begin();
    while(iter != observers.end())
    {
        (*iter++)->update();
        // iter++;
    }
    // for(auto ob : observers)
        // ob->update();
}

feeder::~feeder()
{
    while(observers.size() != 0)
    {
        this->detach(observers.front());
        observers.pop_front();
    }
}