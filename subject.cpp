#include "subject.hpp"

subject::subject()
{
    number = 0;
}

void subject::attach(IObserver* observer)
{
    observers.push_back(observer);
}

void subject::detach(IObserver* observer)
{
    //observers.remove(observer);
    int stopka = 5;
}

void subject::notify()
{
    for(auto ob : observers)
        ob->update();
}

void subject::set_value(int value)
{
    number = value;
}

subject::~subject()
{
    while(observers.size() != 0)
    {
        this->detach(observers.front());
        observers.pop_front();
    }
}