#pragma once

#include "IObserver.hpp"
#include <string>
#include "feeder.hpp"
#include <iostream>

class animal : public IObserver
{
private:
    std::string meno;
    int hlad;
    feeder& _feeder;
public:
    animal(int hlad, std::string meno, feeder& ani);
    ~animal();

    // funkcie pre nas task
    void feed();
    void check_hlad();
    int get_hlad();

    // zdedena funkcia
    void update();

    // moznost definovat operator a vlozit objekt(funktor)
    // priamo do thread bez dalsej funkcie
    // uzitocne ???
    void operator()();
};
