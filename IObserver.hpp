#pragma once

class IObserver
{
private:
public:
    virtual ~IObserver(){};
    virtual void update() = 0;
};
