#include "animal.hpp"

animal::animal(int hlad, std::string meno, feeder& fee):
    _feeder(fee)
{
    fee.registerForFood(this);
    this->hlad = hlad;
    this->meno = meno;
}

void animal::feed()
{
    this->update();
}

void animal::check_hlad()
{
    if(hlad <= 0)
    {
        std:: cout << meno << " odpajam sa" << std::endl;
        _feeder.detach(this);
    }

}

void animal::update()
{
    std::cout << meno << " prave som sa najedol" << std::endl;
    hlad--;
    this->check_hlad();
}

int animal::get_hlad()
{
    return hlad;
}

animal::~animal()
{

}

void animal::operator()()
{

}

