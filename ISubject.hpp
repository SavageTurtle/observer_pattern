#include "IObserver.hpp"
#include <list>


class ISubject
{
private:

protected:
    std::list<IObserver*> observers;
public:
    virtual ~ISubject(){};
    virtual void notify() = 0; // nie je treba parameter, upozorni vsetkych
    virtual void detach(IObserver* observer) = 0;
    virtual void attach(IObserver* observer) = 0;
};

