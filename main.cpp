//Komentar pre git
//Check out na bocnu
//Treti riadok
//Piati riadok
//Siesti riadok

#include <pthread.h>
#include <thread>
#include "feeder.hpp"
#include "animal.hpp"
#include "unistd.h"

using namespace std;

void func_feeder(feeder* feed)
{
    int j = 0;

    // maxmilany pocet nakrmeni
    while(j < 5)
    {
        feed->feed();
        sleep(2);
        j++;
    }
}


void func_animal(feeder* feed, string meno, int hlad)
{
    animal ob(hlad , meno, *feed);
    while(ob.get_hlad() >= 0)
    {
        // animal stale bezi 
    }
}

int main(int argc, char const *argv[])
{
    feeder sub;

    thread th_feeder(func_feeder, &sub);
    thread th_lion(func_animal, &sub, "Lion", 4);
    thread th_turtle(func_animal, &sub, "Turtle", 2);

    // zvierata mozu skoncit skor
    th_lion.detach();
    th_turtle.detach();

    th_feeder.join();

    return 0;
}
