#pragma once

#include "ISubject.hpp"

// #include "animal.hpp"

class feeder : public ISubject
{
private:

public:
    feeder();
    ~feeder();


    // funkcie pre nas task
    void registerForFood(IObserver* an); // attach
    void isFed(IObserver* an); // detach
    void feed(); // notify

    // zdedene funkcie
    void notify(); // nie je treba parameter, upozorni vsetkych
    void detach(IObserver* observer);
    void attach(IObserver* observer);
};