#include "IObserver.hpp"
#include "subject.hpp"
#include <iostream>

class observer : public IObserver
{
private:
    subject& sub; 
public:
    observer(subject& subj);
    virtual ~observer();
    void update();
    void remove_me();
};