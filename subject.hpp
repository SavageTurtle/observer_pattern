#pragma once


#include "ISubject.hpp"

class subject : public ISubject
{
private:
    /* data */
    int number;
public:
    subject();
    ~subject();
    void attach(IObserver* observer);
    void detach(IObserver* observer);
    void notify();
    void set_value(int value);
};
